// _once løber filen igennem én gang, og aldrig igen
// Løber hele DB.php igennem hver gang
// Fordelen er performance-mæssigt, fordi siden hurtigt loader indholdet hvis man bruger _once

<?php 

    class Story
    {

       // public $stories = array (
       //   array("StoryId"=> "1", "StoryDate"=> "13. marts 2011", "StoryName"=>"Opera at the beach", "StoryDescription"=>"Dejlig begivenhed jeg deltog i, og vejret var fantastisk!", "StoryPic" =>"img/about/1.jpg"),
       //   array("StoryId"=> "2", "StoryDate"=> "14. marts 2011", "StoryName"=>"Metal at the beach", "StoryDescription"=>"Dejlig begivenhed jeg deltog i, og vejret var fantastisk!", "StoryPic" =>"img/about/2.jpg"),
       //   array("StoryId"=> "3", "StoryDate"=> "15. marts 2011", "StoryName"=>"Hip Hop at the beach", "StoryDescription"=>"Dejlig begivenhed jeg deltog i, og vejret var fantastisk!", "StoryPic" =>"img/about/3.jpg"),
       //   array("StoryId"=> "4", "StoryDate"=> "16. marts 2011", "StoryName"=>"Techno at the beach", "StoryDescription"=>"Dejlig begivenhed jeg deltog i, og vejret var fantastisk!", "StoryPic" =>"img/about/4.jpg"),
       //   array("StoryId"=> "5", "StoryDate"=> "17. marts 2011", "StoryName"=>"Country at the beach", "StoryDescription"=>"Dejlig begivenhed jeg deltog i, og vejret var fantastisk!", "StoryPic" =>"img/about/1.jpg")
       // );

       // returnAllStoriesView forklarer hvad,  hvor meget og hvor
       // Betyder ikke noget (henter ikke noget)
       // Kunne kalde den jordbær 
       // Brug helst et navn som beskriver handlingen
        function returnAllStoriesView()

        {
            // Lokale variabler til brug længere nede
            // Variablerne bruges til forbindelsen til database serveren
            $servername = "localhost";
            $username ="root";
            $password = "";
            $dbname = "educationdb";

            //Opret forbindelse til databasen
            $conn = new mysqli ($servername, $username, $password, $dbname);
            // Tjek om der er forbindelse
            if ($conn->connect_error) {
                //$connect_error udskriver en beskrivelse af fejlen (hvis der er en fejl)
                    die ("Forbindelsen fejlede: " . $conn->connect_error);
            }

            // En variable der holder på vores SQL DML forespørgsel (DML = Data Manipulation Language)
            // Fortæller at vi gerne vil hente disse oplysninger/indhold fra databasen
            // * betyder alle kolonner i databasen story
            // Hvis man vil formindske antallet af rækker, så anvendes WHERE
            $sql = "SELECT * FROM story"; //

            // query forespørger $sql variablen
            //Forespørgsel gemmes i $result variablen
            // $conn er objektet, som kan eksekvere query
            // Objekter kan eksekvere noget, eller gøre noget 
            $result =$conn->query($sql);

            //Holder øje med hvor mange historier der er i databasen
            $storyCount = $result->num_rows;

            // While løkke der retunerer alle historier fra arrayet
            // Associativt array - behandler hver enkelt række 
            // Associativt array indeholder en key (StoryID, StoryName osv.) og en value (Opera, Metal osv.)
            while($row = $result->fetch_assoc()) {
                $storyCount--;
                // % er modulus, som dividere tallet med to, og hvis tallet er lige med 0, så placeres indholdet på den ene side
                // Vi skal anvende modulus for at kunne sortere nummerisk (så vi kan bruge timeline-inverted)
                if ($storyCount % 2 == 0)
                {
                    echo "<li class='timeline-inverted'>";
                } else {
                    echo "<li>";
                };
                          
                echo "<div class='timeline-image'>"; 
                echo "<img class='img-circle img-responsive' src='../resources/img/" . $row["StoryImg"] . "' alt=''>";
                echo "</div>";
                // Skaber et dynamisk link, som sender brugeren videre til den pågældende historie (med Id'et)')
                echo "<a href='storydetail.php?id=" . $row["StoryID"] . "'>"; 
                echo "<div class='timeline-panel'>";
                echo "<div class='timeline-heading'>";
                echo "<h4>" . $row["StoryData"] . "</h4>";
                echo "<h4 class='subheading'>" . $row["StoryName"] . "</h4>";
                echo "</div>";
                echo "<div class='timeline-body'>";
                echo "<p class='text-muted'>" . $row["StoryDescription"] . "</p>";
                echo "</div>";
                echo "</a>";
                echo "</div>";
                echo "</li>";
            
             }
        }
         function returnStoryDetailView($storyid)

        {
            // Lokale variabler til brug længere nede
            // Variablerne bruges til forbindelsen til database serveren
            $servername = "localhost";
            $username ="root";
            $password = "";
            $dbname = "educationdb";

            //Opret forbindelse til databasen
            $conn = new mysqli ($servername, $username, $password, $dbname);
            // Tjek om der er forbindelse
            if ($conn->connect_error) {
                //$connect_error udskriver en beskrivelse af fejlen (hvis der er en fejl)
                    die ("Forbindelsen fejlede: " . $conn->connect_error);
            }

            // En variable der holder på vores SQL DML forespørgsel (DML = Data Manipulation Language)
            // Fortæller at vi gerne vil hente disse oplysninger/indhold fra databasen
            // * betyder alle kolonner i databasen story
            // Hvis man vil formindske antallet af rækker, så anvendes WHERE
            $q = "SELECT * FROM story WHERE StoryID = $storyid";

            // query betyder forespørger
            $result = $conn->query($q);
            
            // Denne anvender vi, når vi kun ønsker at udskrive én række 
            $value = mysqli_fetch_assoc($result);
            echo "<h2>" . $value["StoryName"] . "</h2>";
            echo "<figure>";
            echo "<img class='img-responsive img-centered figure-img img-fluid img-rounded' src='../resources/img/" . $value["StoryImg"] . "' alt=''>";
            echo "<figcaption class='figure-caption'>" . $value["StoryImgCaption"] . "</figcaption>";
            echo "</figure>";
            echo "<br>";
            echo "<p>" . $value["StoryDescription"] . "</p>";

        }

        function postNewStory($storyname, $storydescription, $storydata)
        {

            // Lokale variabler til brug længere nede
            // Variablerne bruges til forbindelsen til database serveren
            $servername = "localhost";
            $username ="root";
            $password = "";
            $dbname = "educationdb";

            //Opret forbindelse til databasen
            $conn = new mysqli ($servername, $username, $password, $dbname);
            // Tjek om der er forbindelse
            if ($conn->connect_error) {
                //$connect_error udskriver en beskrivelse af fejlen (hvis der er en fejl)
                    die ("Forbindelsen fejlede: " . $conn->connect_error);
            }

            $q = "INSERT INTO story(StoryName) VALUES ('$storyname')";
            // $result bruges ikke her, fordi vi ikke forventer at modtage nogle data 
            $conn->query($q);

        }

    }


?>
