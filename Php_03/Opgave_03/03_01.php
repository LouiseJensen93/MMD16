<?php
    /*
     * Opgave 03_02
     * 
     * Metoden metoden extractValuesFromArray skal kunne hente værdierne fra et indexeret array. 
     * Brug den indbyggede metode list() til at udtrække værdierne.
     * Se kapitel - Array -> Extracting multiple values.
     */
    
    class Person
    {
        function extractValuesFromArray()
        {
            $someRandomPerson = array("Fred", 35, "Betty");
            list($name , $age , $wife)= $someRandomPerson; 
            echo  $name , $age , $wife;

            
        }
    }
    
    $person = new Person;
    $person->extractValuesFromArray();
    
?>  