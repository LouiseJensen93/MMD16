<?php
    /*
     * Opgave 03_01
     * 
     * En event er en begivendhed der er afgrænset ved tid og sted, og som er kendetegnet ved specifikt indhold.
     * 
     * Opgave 1) 
     * Find nogle flere events. Søg inspiration på http://www.visitskive.dk/skive/begivenheder-2
     * Indsæt de fundne events i $events arrayet nede i klassen Event. Find latitude og longitude på https://itouchmap.com/latlong.html
     *
     * Opgave 2)
     * I klassen Event er der en tom metode der hedder getAllEvents. Metoden skal returnere hele $events arrayet fra klassen.
     * Når vi returnerer hele arrayet, så kan vi anvende det andre steder. Fx i index.php
     *
     * Opgave 3)
     * Gå til index.php
     * Lokaliser der hvor markører bliver indsat på kortet. Erstat de to eksisterende markører med php kode. Koden skal løbe igennem det array, som metoden getAllEvents 
     * returnerer og udskrive indholdet, således, at hver gang løkken løber en gang, vises en markør på kortet.
     * 
     * 
     */
    
    class Event
    {
        private $events = array(
            array(
            "EventId"=>1,
            "EventName"=>"Limfjorden Rundt",
            "EventDescription"=>"For alle personer",
            "EventDate"=>"17 september 2016 10:00am",
            "Lat"=>"56.574344",
            "Long"=>"9.051586",
            "EventImage"=>"img/ship.png"
        ),
        array(
            "EventId"=>2,
            "EventName"=>"Oyster Trophy Week",
            "EventDescription"=>"For alle der elsker østers.",
            "EventDate"=>"19 oktober 2016 10:00am",
            "Lat"=>"56.756884",
            "Long"=>"8.875710",
            "EventImage"=>"img/opera.png"
        ),
        array(
            "EventId"=>3,
            "EventName"=>"Limfjordsfortællinger, Viborg",
            "EventDescription"=>"For alle",
            "EventDate"=>"31 august 2016 2:00am",
            "Lat"=>"56.452027",
            "Long"=>"9.396347",
            "EventImage"=>"img/metal.png"
        ));
        function __construct()
        {
            //Konstruktør (funktionen) skal ikke benyttes
        }
        function getAllEvents()
        {

            foreach($this->events as $ev)
            {
                $keys = array_keys($ev);
                echo  'L.marker([' . $ev["Lat"] . ',' . $ev["Long"] . ']).addTo(mymap).bindPopup("' . $ev["EventDescription"] . '");';
                
            }

            //Denne funktion skal returnere alle event i events arrayet
        }
        
    }

     
?>