<?php include("../include/story.php"); ?>

<!-- About Section -->
    <section id="about">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h2 class="section-heading">Historier</h2>
                    <h3 class="section-subheading text-muted">Del og læs hvilke oplevelser der har fundet sted.</h3>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <ul class="timeline">

                    <?php

                        $story = new story;
                        $story->returnAllStoriesView();

                    ?>
                        
                    </ul>
                </div>
            </div>
        </div>
    </section>