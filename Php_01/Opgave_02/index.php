    

<!doctype>
    <!--
     * Array
     * Opsæt et accosiativt array der indeholder byer og deres hovedstader.
     * Der skal minimum være 20 lande og deres respektive hovedstader i arrayet.
     * "Italy"=>"Rome"
     * Når Arrayet er opsat, skal du udskrive arrayet tre gange.
     * 1. gang skal arrayet udskrives i den rækkefølge, som angivet i arrayet.
     * 2. gang skal arrayet udskrives, således, at landenavne er sorteret efter forbogstav. Lande med A i forbogstav skal komme først.
     * 3. gang skal arrayet udskrives, således, at bynavnen er sorteret efter forbogstav.
     * Få hjælp her: http://php.net/manual/en/array.sorting.php
     * I mappen ligger landeOgByer.png hvor du kan se udskriften for arrayet der ikke er sorteret.
     -->

<head>
    <title>Lande og hovedstæder</title>
    <meta charset="utf-8">
    <!-- Reference til bootstrap -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
</head>
<body>

    <?php

    $countries = array( "Italy"=>"Rome", "Denmark" => "Copenhagen", "Sweden" => "Stockholm", "Norway" => "Oslo", "Finland" => "Helsinki", "France" => "Paris", "Italy" => "Rome", "Luxembourg" => "Luxembourg", "Belgium" => "Brussels", "Slovakia" => "Bratislava", "Slovenia" => "Ljubljana", "Germany" => "Berlin", "Greece" => "Athens", "Ireland" => "Dublin", "Netherlands" => "Amsterdam", "Spain" => "Madrid", "United Kingdom" => "London", "Portugal" => "Lisabon", "Cyprus" => "Nicosia", "Lithiania" => "Vilnius", "Czech Republic" => "Prague", "Estonia" => "Tallin", "Hungary" => "Budapest", "Latvia" => "Riga", "Malta" => "Valetta", "Austria" => "Vienna", "Poland" => "Warsaw");

     $countries += array("Russia" => "Moscow");

    ?>

    <div class="container">
        <div class="row">
            <div class="col-md-3">
                <h2>Lande og hovedstæder</h2>
                
                <?php

                
                 foreach ($countries as $land => $hovedstad) {
                     echo "" . $land . ", " . $hovedstad . "<br>";
                    
                }

                ?>

            </div>
            <div class="col-md-3">
                <h2>Lande</h2>

                <?php 

                ksort ($countries);
                foreach ($countries as $land => $hovedstad) {
                    echo "" . $land . "<br>";
                }


                ?>

                
               
            </div>
            <div class="col-md-3">
                <h2>Hovedstæder</h2>

                <?php
                
                asort ($countries);
                foreach ($countries as $land => $hovedstad) {
                    echo "" . $hovedstad . "<br>";
                }

                ?>
    

            </div>
            
        </div>
    </div>

    <!--
     * Ekstra opgave
     * Tilføj flere elementer til arrayet. Brug array_push()
     -->

     

    
</body>