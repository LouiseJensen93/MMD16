<?php
    /* Dette dokument skal indeholde de dataelementer der indgår i html siden.
     * Følgende dataelementer skal som minimum være udpenslet i dette dokument.:
     * - email 
     * - phone
     * - preferences
     * - jobs
     * - competencies
     *
     * Det vil være naturligt at lade ovenstående elementer være 
     */

     
     $email = "loui950p@student.eadania.dk";
     $phone = 20709249;

     $preferences = array('Programmering','Organisation og udvikling','Kommunikation', 'Markedsføring', 'Interaktionsudvikling', 'Grafisk design', 'Website og App-udvikling');

     $jobs = array('Skive By The Shore' => 'Hjemmeside','Jysk Gas & VVS teknik' => 'Logo og designmanual','Skives 11 Stjerner' => 'App');

     
     $competencies = array('Undervisning',array('PHP','HTML','CSS', 'Javascript'), array('Adobe Photoshop','Adobe Illustrator','Adobe InDesign', 'Adobe Premiere Pro', 'Adobe After Effects'));

?>