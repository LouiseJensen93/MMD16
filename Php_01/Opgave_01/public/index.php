<!doctype>

<head>
    <title>Min kontaktside</title>
    <meta charset="utf-8">
    <!-- Reference til bootstrap -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
</head>
<body>

<?php include ("../include/data.php") ?>
    <div class="container">
        <div class="row">
            <div class="col-md-3">
                <h2>Resume</h2>
                
                <p>Mit navn er Louise Jensen, og jeg er studerende på Erhvervs Akademi Dania, Skive. Jeg studerer til multimediedesigner, på 3. semester.</p>
                
                <h4>Kontaktoplysninger</h4> 

                <p>Email: <?php echo $email ?></p>
                <p>Telefon: <?php echo $phone ?></p>

            </div>
            <div class="col-md-3">
                <h2>Præferencer</h2>

                <?php 
                    foreach ($preferences as $preference) {
                        echo "<li>" . $preference . "</li>";

                    }

                ?>
               
            </div>
            <div class="col-md-3">
                <h2>Portefølje</h2>

                <?php 
                    foreach ($jobs as $employer => $job) {
                            echo "<li>" . $employer . " <span style='color:#aaa;'>(". $job . ")</span>". "</li>";
                    }
                ?>

    

            </div>
            <div class="col-md-3">
                <h2>Kompetencer</h2>

                <?php

                foreach ($competencies as $comp){

                     if (is_array($comp))  {
                        
                        foreach ($comp as $c) {
                                echo "<li>" . $c . "</li>";}
                    }else {
                        echo "<li>" . $comp . "</li>";

                    }
                    } 
                

                ?>

                
            </div>
        </div>
    </div>


    
</body>