<?php
    // Datatyper og variabler. Dette dokument indlejres i det efterfølgende

    //tekststreng (string)
    $firstName = "Louise";

    //tekststreng (string)
    $lastName = "Jensen";

    //nummerisk heltal (integer)
    $age = 22;

    //boolean (sandt/falsk)
    $inRelationship = true; 

    //tekststreng (string)
    $work = "studerende";

    //tekststreng (string)
    $workPlace = "Erhvervsakademi Dania, Skive";

    //array (en række)
    $hobbies = ["grafisk design", "madlavning", "film og serier"];
    
    ?> 