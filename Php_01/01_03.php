<!-- Hvordan vi indlejre indhold fra andre filer -->
<!doctype html>
<html>
    <head>
        <meta charset='utf-8'>
        <title>Indlejre andre filer</title>
    </head>
    <body>
        <p>1</p>
        <p>2</p>
        <p>3</p>
        <?php 
            include("numbers.php");
        ?>
        <p>7</p>
        <p>8</p>
        <p>9</p>
    </body>
</html>