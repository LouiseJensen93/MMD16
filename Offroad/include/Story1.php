<?php 

    class Story
    {

        public $stories = array (
            array("StoryId"=> "1", "StoryDate"=> "13. marts 2011", "StoryName"=>"Opera at the beach", "StoryDescription"=>"Dejlig begivenhed jeg deltog i, og vejret var fantastisk!", "StoryPic" =>"img/about/1.jpg"),
            array("StoryId"=> "2", "StoryDate"=> "14. marts 2011", "StoryName"=>"Metal at the beach", "StoryDescription"=>"Dejlig begivenhed jeg deltog i, og vejret var fantastisk!", "StoryPic" =>"img/about/2.jpg"),
            array("StoryId"=> "3", "StoryDate"=> "15. marts 2011", "StoryName"=>"Hip Hop at the beach", "StoryDescription"=>"Dejlig begivenhed jeg deltog i, og vejret var fantastisk!", "StoryPic" =>"img/about/3.jpg"),
            array("StoryId"=> "4", "StoryDate"=> "16. marts 2011", "StoryName"=>"Techno at the beach", "StoryDescription"=>"Dejlig begivenhed jeg deltog i, og vejret var fantastisk!", "StoryPic" =>"img/about/4.jpg"),
            array("StoryId"=> "5", "StoryDate"=> "17. marts 2011", "StoryName"=>"Country at the beach", "StoryDescription"=>"Dejlig begivenhed jeg deltog i, og vejret var fantastisk!", "StoryPic" =>"img/about/1.jpg")
        );

        function returnAllStories()

        {

            //Funktion der retunere alle historierne fra arrayet.
            $counter = 0;
            foreach ($this->stories as $story)

            {
                $class = ''; 

                if(++$counter % 2 === 0) {
                    $class = 'timeline-inverted';
                }              

                echo '<li class="' . $class . '">';
                echo '<div class="timeline-image">'; 
                echo '<img class="img-circle img-responsive" src="' . $story["StoryPic"] . '" alt="">';
                echo '</div>';
                echo '<div class="timeline-panel">';
                echo '<div class="timeline-heading">';
                echo '<h4>' . $story["StoryDate"] . '</h4>';
                echo '<h4 class="subheading">' . $story["StoryName"] . '</h4>';
                echo '</div>';
                echo '<div class="timeline-body">';
                echo '<p class="text-muted">' . $story["StoryDescription"] . '</p>';
                echo '</div>';
                echo '</div>';
                echo '</li>';
            }

        }
    }


?>
