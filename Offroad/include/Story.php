<?php

// Lokale variabler til brug længere nede
// Variablerne bruges til forbindelsen til database serveren
$servername = "localhost";
$username ="root";
$password = "";
$dbname = "educationdb";

//Opret forbindelse
$conn = new mysqli ($servername, $username, $password, $dbname);
// Tjek om der er forbindelse
if ($conn->connect_error) {
    //$connect_error udskriver en beskrivelse af fejlen (hvis der er en fejl)
    die ("Forbindelsen fejlede: " . $conn->connect_error);
}

// En variable der holder på vores SQL DML forespørgsel (DML = Data Manipulation Language)
$sql = "SELECT * FROM story"; //

//Forespørgsel gemmes i $result variablen
$result =$conn->query($sql);

//Udskriver data (hvis der er noget i $result)
if ($result->num_rows > 0) {
    // Output data fra hver række
    while ($row = $result->fetch_assoc()) {
        echo "Navn: " . $row["StoryName"] . " <br> Beskrivelse: " . $row["StoryDescription"] . "<br> Dato og tid: " . $row["StoryData"] . "<br><br><img width= 800px src='" . $row["StoryImg"] ."'>" . "<br>";
    
    }
} else {
    echo "Ingen resultater fra databasen";
}

// Lukker forbindelsen
$conn->close();
    
?>