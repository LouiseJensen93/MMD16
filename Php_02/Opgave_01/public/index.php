<!doctype html>
<html>
    <head> 
        <title></title>
        <meta charset="utf-8">
        <!-- Reference til bootstrap -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    </head>
    <body>
    <?php include("../include/basket.php"); ?>
    <?php include("../include/register.php"); ?>
    <div class="container">
        <div class="row"> 
            <h3>Udregn og returner prisen for varen inklusiv moms. </h3>
            <p>Indtast prisen uden moms</p>
            <h4><?php echo returnPriceInclusiveVAT(80); ?></h4>
        </div>
        <hr>
        <div class="row">
            <h3>Udregn og returner momsen på varen.</h3>
            <p>Indtast prisen uden moms</p>
            <h4><?php echo returnDeductedVAT(100); ?></h4>
        </div>        
        <hr>
        <div class="row">
            <h3>Udregn prisen eksklusiv moms</h3>
            <p>Indtast prisen inklusiv moms</p>
            <h4><?php echo returnPriceExcludingVAT(100); ?></h4>
        </div>
        <div class="row">
            <h3>Udregn prisen for varen, når momsen er opgivet</h3>
            <p>Indtast momsen</p>
            <h4><?php echo returnPriceFromVAT(20); ?></h4>
        </div>
        <div class="row">
            <h3>Udregn prisen eksklusiv moms</h3>
            <p>Indtast prisen inklusiv moms</p>
            <h4><?php echo "Pris DKK " . returnPriceWithDiscount(1000, 50, 1.25) . ",-"; ?></h4>
            <p>Prisen er inklusiv moms og rabat</p>
            <p>Moms udgør <?php echo returnDeductedVAT(returnPriceWithDiscount(1000, 50, 1.25)) ?>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <h3>Tilføj produkt til kurv</h3>
            <h4>
                <?php
                    $b= new Basket;
                    $watermelon =
                    $p = $b->addProductToBasket($watermelon);
                    var_dump($p);
                ?>
            </h4>
        </div>
        <div class="row">
            <h3>Fjern produkt fra kurv</h3>
            <h4>
                <?php
                    $b = new Basket;
                    $p = $b->removeProductFromBasket(1);
                    var_dump($p);

                ?>
            </h4>
        </div>
        <div class="row">
            <h3>Total antal produkter i kurven</h3>
            <h4>
                <?php
                    $b = new Basket;
                    $p = $b->returnQuantityInBasket();
                    var_dump($p);

                ?>
            </h4>
        </div>
        <div class="row">
            <h3>Total prisen for varerene i kurven</h3>
            <h4>
                <?php
                    $b = new Basket;
                    $p = $b->returnTotalPriceInBasket();
                    var_dump($p);
                ?>
            </h4>
        </div>
    </div>
    </body>
</html>