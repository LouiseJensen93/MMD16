<?php

    /*
    * Flow kontrol.
    * 
    * While
    * 
    */


/*  
    I vores eventyr er der masser af mellemrum, men hvor mange er der? 
    Vores while løkke itererer over alle karaktere og tæller dem 
*/
$eventyr = "Der var engang en lille pige. Hver eftermiddag gav hendes mor hende en lille skål med mælk og tvebakker og så satte hun sig ud i gården. Når hun begyndte at spise, kom en skrubtudse kravlende ud af en revne i muren, stak hovedet ned i mælken og spiste med. Barnet morede sig derover, og når skrubtudsen ikke kom straks, når hun havde sat sig derude med sin lille skål, råbte hun: Skynd dig, lille tudse, kom du lidt mælk og brød skal have, du er sulten vist, og tom er din lille mave.";

//Dette giver os et array med alle ovenstående ord
$words = explode(" ", $eventyr);
$wordToFind = 'hun';
$counter = 0;
$wordcount = count($words);

while ($wordcount > 0) {
    $wordcount--;
    if($words[$wordcount] == $wordToFind)
    {
        $counter++;
    }
    
}
echo "Antal instanser: $counter <br>";


/*
    Følgende funktionalitet ligger en aktuel karakter til en variable. Når karakteren er et mellemrum,
    hopper udskriver den substringen og begynder forfra.
*/
$eventyr = "Der var engang en lille pige. Hver eftermiddag gav hendes mor hende en lille skål med mælk og tvebakker og så satte hun sig ud i gården. Når hun begyndte at spise, kom en skrubtudse kravlende ud af en revne i muren, stak hovedet ned i mælken og spiste med. Barnet morede sig derover, og når skrubtudsen ikke kom straks, når hun havde sat sig derude med sin lille skål, råbte hun: Skynd dig, lille tudse, kom du lidt mælk og brød skal have, du er sulten vist, og tom er din lille mave.";

$stringlenght = strlen($eventyr);
$substring = "";
$counter = 0;
$girlcounter = 0;
while ($stringlenght > 0) {
    //Er følgende karakter en mellemrum? Hvis ikke, så skriv karakteren til substring. Hvis nej, udskriv substring og et mellemrum.
    if($eventyr[$counter] != " "){
        $substring .=  $eventyr[$counter];
    }else {
        $substring = "";
    }
    //Står der 'pige' i substring? Hvis ja, så 
    if($substring == "hun")
    {
        $girlcounter++;
    }
    $counter++;
    $stringlenght--;
}
echo $girlcounter;