<?php

    /*
    * Funktioner.
    * 
    * Hvordan vi opsplitter funktionalitet i logisk entiteter
    * 
    */

    //Denne funktion tager en textstreng og smider hvert ord i et array
    function addWordsToArray($textString){
        $wordsArray = explode(" ", $textString);
        return $wordsArray;
    }

    //Denne funktion tager et array med ord, 
    //og et ord der skal findes i arrayet og returnerer antallet af instanser.
    function returnNumOfInstances($wordsArray, $wordToFind){
        $wordcount = count($wordsArray);
        $counter = 0;
        while ($wordcount > 0) {
            $wordcount--;
            if ($wordsArray[$wordcount] == $wordToFind) {
                $counter++;
            }
        }
        return $counter;
    }


    $eventyr = "Der var engang en lille havfrue, der ikke var så lille alligevel!";

    $arrayOfWords = addWordsToArray($eventyr);
    echo returnNumOfInstances($arrayOfWords, "lille");
?>