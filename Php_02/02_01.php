<?php

    /* 
     * Operatorer
     * 
    */

    // Addition (tildelings-operator)

    $a = 1+2;
    echo $a;
    echo "<br>";

    // To variablers værdier adderet 

    $b = 2;
    $c = 2;
    echo $b+$c;
    echo "<br>";

    // Subtraktion

    $a = 1-2;
    echo $a; 
    echo "<br>";

    //To variablers værdier subtrakteret 

    $b = 2;
    $c = 2;
    echo $b-$c;
    echo "<br>";


    // Multiplikation 

    $a = 2*2;
    echo $a; 
    echo "<br>";

    //To variablers værdier multiplikeret 

    $b = 2;
    $c = 2;
    echo $b*$c;
    echo "<br>";

?>